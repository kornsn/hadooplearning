package ks.hadooplearning.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Reducer class
 */
public class Reduce extends Reducer<Text, IntWritable, Text, LongWritable> {
    private final LongWritable result = new LongWritable();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        long s = 0;
        for (IntWritable v : values) {
            s += v.get();
        }
        result.set(s);
        context.write(key, result);
    }
}
