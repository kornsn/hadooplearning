#!/bin/bash

DRIVER=ks.hadooplearning.wordcount.Driver
LIBJARS=$(ls -d -1 $PWD/lib/*.jar | tr "\\n" "," | sed 's/,$//')

export HADOOP_CLASSPATH=`echo ${LIBJARS} | sed s/,/:/g`

yarn jar $PWD/driver/*.jar ${DRIVER} -libjars ${LIBJARS} "$@"
