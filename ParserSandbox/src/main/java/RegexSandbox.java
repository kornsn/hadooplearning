import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ks on 1/13/15.
 */
public class RegexSandbox {
    private static final Pattern pattern = Pattern.compile
            ("^\\s*(\\d+),\"(.*)\",\"(.*)\",\"(.*)\",\"(.*)\",(\\d+),\"(.*)\",\"(.*)\",\"(.*)\",\"(.*)\",(.*),(\\d*\\.?\\d*),(\\d*\\.?\\d*)\\s*$");

    private static final String testString = "1,\"Алтайский край\",\"Благовещенский муниципальный район\",\"658672, р.п. Благовещенка, ул. Ленина, 97\",\"Краевое автономное учреждение \"Благовещенский филиал краевого автономного учреждения \"\",7,\"Тишин Денис Владимирович\",\"http://mfc22.ru/\",\"8 (385) 642-3965\",\"mfc@mfc22.ru\",,,";

    public static void main(String[] args) {
        Matcher matcher = pattern.matcher(testString);
        if (matcher.find()) {
            for (int i = 1, n = matcher.groupCount(); i <= n; i++) {
                System.out.println("group " + i + ": " + matcher.group(i));
            }
        }
    }
}
