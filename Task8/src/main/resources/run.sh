#!/bin/bash

DRIVER=ks.hadooplearning.task8.mapreduce.Driver
LIBJARS=$(ls -d -1 $PWD/lib/*.jar | tr "\\n" ":" | sed 's/:$//')

export HADOOP_CLASSPATH=`echo ${LIBJARS}`

yarn jar $PWD/driver/*.jar ${DRIVER} -libjars ${LIBJARS} "$@"
