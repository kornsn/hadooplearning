package ks.hadooplearning.task8.mapreduce.stage10

import java.lang.Iterable

import ks.hadooplearning.task8.schemas.PhmDealerFranchiseSchema._
import ks.hadooplearning.task8.utils.ScalaExts._
import org.apache.hadoop.hbase.client.{Mutation, Put}
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapreduce.TableReducer
import org.apache.hadoop.io.{BytesWritable, IntWritable}
import org.apache.hadoop.mapreduce.Reducer

import scala.collection.JavaConversions._

/**
 * Created by ks on 12.02.15.
 */
class ScalaJoinReduce extends TableReducer[IntWritable, BytesWritable, ImmutableBytesWritable] {

  override def reduce(
                       key: IntWritable,
                       values: Iterable[BytesWritable],
                       context: Reducer[IntWritable, BytesWritable, ImmutableBytesWritable, Mutation]#Context)
  = {
    val bytes = values.map(_.getBytes)
    val fs = bytes
      .withFilter(_(0) == 0)
      .map(_.drop(1).deserializeF);
    val rs = bytes
      .withFilter(_(0) == 1)
      .map(_.drop(1).deserializeR);

    for (f <- fs;
         r <- rs
    ) {
      val key = f.getDealerFranchiseId
      val put: Put = new Put(key)
        .add(BIN_fAMILY, dealer_franchise_id, f.getDealerFranchiseId)
        .add(BIN_fAMILY, dealership_id, f.getRooftopLocationId)
        .add(BIN_fAMILY, rooftop_name, r.getRooftopName);
      context.write(new ImmutableBytesWritable(key), put);
    }
  }
}
