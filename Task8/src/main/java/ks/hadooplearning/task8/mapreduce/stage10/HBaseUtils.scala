package ks.hadooplearning.task8.mapreduce.stage10

import ks.hadooplearning.task8.model.{MainFDealerFranchiseVer, MainFDealerRooftop}
import ks.hadooplearning.task8.utils.ScalaExts._
import org.apache.hadoop.hbase.client.Result


/**
 * Created by ks on 13.02.15.
 */
object HbaseUtils {

  implicit class Scan(val value: Result) {
    def scanF: MainFDealerFranchiseVer = {
      import ks.hadooplearning.task8.schemas.MainFDealerFranchiseVerSchema._
      return MainFDealerFranchiseVer
        .newBuilder
        .setDealerFranchiseId(value.getValue(IN_fAMILY, dealer_franchise_id))
        .setRooftopLocationId(value.getValue(BIN_fAMILY, rooftop_location_id))
        .setParentDealerFranchiseId(value.getValue(BIN_fAMILY, parent_dealer_franchise_id))
        .setEffEndDatetime(value.getValue(BIN_fAMILY, eff_end_datetime))
        .build()
    }

    def scanR: MainFDealerRooftop = {
      import ks.hadooplearning.task8.schemas.MainFDealerRooftopSchema._
      return MainFDealerRooftop
        .newBuilder
        .setRooftopLocationId(value.getValue(BIN_fAMILY, rooftop_location_id))
        .setRooftopName(value.getValue(BIN_fAMILY, rooftop_name))
        .build()
    }
  }

}
