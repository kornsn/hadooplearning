package ks.hadooplearning.task8.mapreduce.stage10;

import ks.hadooplearning.task8.model.MainFDealerRooftop;
import ks.hadooplearning.task8.utils.AvroSerializer;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;

import java.io.IOException;

import static ks.hadooplearning.task8.schemas.MainFDealerRooftopSchema.*;

/**
 * Created by ks on 09.02.15.
 */
public class Map1 extends TableMapper<IntWritable, BytesWritable> {

    private final IntWritable keyOut = new IntWritable();
    private final BytesWritable valueOut = new BytesWritable(new byte[]{1});
    private final AvroSerializer serializer = new AvroSerializer(MainFDealerRooftop.getClassSchema());

    @Override
    protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
        keyOut.set(Bytes.toInt(value.getRow()));
        MainFDealerRooftop.Builder builder = MainFDealerRooftop.newBuilder();

        builder.setRooftopLocationId(Bytes.toInt(value.getValue(BIN_fAMILY, rooftop_location_id)));
        builder.setRooftopName(Bytes.toString(value.getValue(BIN_fAMILY, rooftop_name)));
        byte[] serialized = serializer.serialize(builder.build());
        valueOut.set(serialized, 1, serialized.length);
        context.write(keyOut, valueOut);
    }
}
