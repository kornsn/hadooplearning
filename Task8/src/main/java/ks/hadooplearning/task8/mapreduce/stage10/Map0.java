package ks.hadooplearning.task8.mapreduce.stage10;

import ks.hadooplearning.task8.model.MainFDealerFranchiseVer;
import ks.hadooplearning.task8.utils.AvroSerializer;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;

import java.io.IOException;
import java.sql.Date;

import static ks.hadooplearning.task8.schemas.MainFDealerFranchiseVerSchema.*;

/**
 * Created by ks on 09.02.15.
 */
public class Map0 extends TableMapper<IntWritable, BytesWritable> {

    public static final long TIME = Date.valueOf("9999-12-31 00:00:00").getTime();
    private final IntWritable keyOut = new IntWritable();
    private final BytesWritable valueOut = new BytesWritable(new byte[]{0});
    private final AvroSerializer serializer = new AvroSerializer(MainFDealerFranchiseVer.getClassSchema());

    @Override
    protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
        keyOut.set(Bytes.toInt(value.getRow()));
        MainFDealerFranchiseVer.Builder builder = MainFDealerFranchiseVer.newBuilder();

        // filter
        byte[] parentDealerFranchiseId = value.getValue(BIN_fAMILY, parent_dealer_franchise_id);

        if (parentDealerFranchiseId == null) {
            long effEndDatetime = Bytes.toLong(value.getValue(BIN_fAMILY, eff_end_datetime));

            if (effEndDatetime == TIME) {
                builder.setDealerFranchiseId(Bytes.toInt(value.getValue(BIN_fAMILY, dealer_franchise_id)));
                builder.setRooftopLocationId(Bytes.toInt(value.getValue(BIN_fAMILY, rooftop_location_id)));
                builder.setParentDealerFranchiseId(Bytes.toInt(parentDealerFranchiseId));
                builder.setEffEndDatetime(effEndDatetime);
                byte[] serialized = serializer.serialize(builder.build());
                valueOut.set(serialized, 1, serialized.length);
                context.write(keyOut, valueOut);
            }
        }
    }
}
