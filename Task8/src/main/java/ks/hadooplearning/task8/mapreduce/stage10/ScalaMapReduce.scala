package ks.hadooplearning.task8.mapreduce.stage10

import java.text.SimpleDateFormat

import ks.hadooplearning.task8.mapreduce.stage10.HbaseUtils._
import ks.hadooplearning.task8.model.{MainFDealerFranchiseVer, MainFDealerRooftop, PhmDealerFranchise}
import ks.hadooplearning.task8.schemas.{MainFDealerFranchiseVerSchema, MainFDealerRooftopSchema, PhmDealerFranchiseSchema}
import ks.hadooplearning.task8.utils.ScalaExts._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapreduce.TableInputFormat
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by ks on 13.02.15.
 */
object ScalaMapReduce {

  val TIME: Long = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss")
    .parse("9999-12-31 00:00:00")
    .getTime

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setAppName("HBaseTest")
    val sc = new SparkContext(sparkConf)
    val conf = HBaseConfiguration.create()

    val tConf1: Configuration = HBaseConfiguration.create()
    tConf1
      .set(TableInputFormat.INPUT_TABLE, MainFDealerFranchiseVerSchema.TABLE_NAME.getNameAsString)

    "2".toInt

    val hBaseRdd1 = sc.newAPIHadoopRDD(
      tConf1,
      classOf[TableInputFormat],
      classOf[ImmutableBytesWritable],
      classOf[Result]
    )

    val tConf2: Configuration = HBaseConfiguration.create()
    tConf2
      .set(TableInputFormat.INPUT_TABLE, MainFDealerRooftopSchema.TABLE_NAME.getNameAsString)

    val hBaseRdd2: RDD[(ImmutableBytesWritable, Result)] = sc.newAPIHadoopRDD(
      tConf2,
      classOf[TableInputFormat],
      classOf[ImmutableBytesWritable],
      classOf[Result]
    )



    hBaseRdd2.cache();

    val conn: Connection = ConnectionFactory.createConnection(conf)
    val table: Table = conn.getTable(PhmDealerFranchiseSchema.TABLE_NAME)


    val join =
      hBaseRdd1
        .map(t => (t._1, t._2.scanF))
        .filter(t => t._2.getParentDealerFranchiseId == null
        && t._2.getEffEndDatetime == TIME)
        .join(
          hBaseRdd2
            .map(t => (t._1, t._2.scanR)))

    val store = join.foreach((tuple: (ImmutableBytesWritable, (MainFDealerFranchiseVer, MainFDealerRooftop))) => {
      val f: MainFDealerFranchiseVer = tuple._2._1
      val r: MainFDealerRooftop = tuple._2._2
      table.put(PhmDealerFranchise
        .newBuilder()
        .setDealerFranchiseId(f.getDealerFranchiseId)
        .setDealershipId(f.getRooftopLocationId)
        .setRooftopName(r.getRooftopName)
        .build())
    })

    val scs: StreamingContext = new StreamingContext(sc, Seconds(1))

    scs.start()

    conn.close
  }

  implicit def toPut(d: PhmDealerFranchise): Put = {
    import ks.hadooplearning.task8.schemas.PhmDealerFranchiseSchema._
    new Put(d.getDealerFranchiseId)
      .add(BIN_fAMILY, dealer_franchise_id, d.getDealerFranchiseId)
      .add(BIN_fAMILY, dealership_id, d.getDealershipId)
      .add(BIN_fAMILY, rooftop_name, d.getRooftopName)
  }


}

