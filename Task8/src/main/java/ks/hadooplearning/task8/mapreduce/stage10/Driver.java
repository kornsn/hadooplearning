package ks.hadooplearning.task8.mapreduce.stage10;

import ks.hadooplearning.task8.schemas.MainFDealerFranchiseVerSchema;
import ks.hadooplearning.task8.schemas.MainFDealerRooftopSchema;
import ks.hadooplearning.task8.schemas.PhmDealerFranchiseSchema;
import ks.hadooplearning.task8.utils.HBaseUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by ks on 12.02.15.
 */
public class Driver extends Configured implements Tool {

    private static final String JOB_NAME = "Task8.Stage10";

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new Driver(), args));
    }

    private static void createOutTable(Configuration configuration) throws IOException {
        HBaseUtils utils = new HBaseUtils(configuration).Initialize();
        if (utils.existsTable(PhmDealerFranchiseSchema.TABLE_NAME)) {
            utils.dropTable(PhmDealerFranchiseSchema.TABLE_NAME);
        }
        utils.createTable(PhmDealerFranchiseSchema.TABLE_DESCRIPTOR);
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 0) {
            System.err.println("Usage: " + getClass().getName());
            return 1;
        }

        Configuration conf = getConf();

        // workaround for error "client.ZooKeeperRegistry: ClusterId read in ZooKeeper is null"
        // see http://2scompliment.wordpress.com/2013/12/11/running-hbase-java-applications-on-hortonworks-hadoop-sandbox-2-x-with-yarn/
        conf.set("zookeeper.znode.parent", "/hbase-unsecure");

        Job job = Job.getInstance(conf, JOB_NAME);
        job.setJarByClass(getClass());
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(BytesWritable.class);

        HBaseUtils utils = new HBaseUtils(conf).Initialize();

        createOutTable(conf);

        Scan scan0 = new Scan()
                .addFamily(MainFDealerFranchiseVerSchema.BIN_fAMILY)
                .addColumn(MainFDealerFranchiseVerSchema.BIN_fAMILY, MainFDealerFranchiseVerSchema.dealer_franchise_id)
                .addColumn(MainFDealerFranchiseVerSchema.BIN_fAMILY, MainFDealerFranchiseVerSchema.rooftop_location_id)
                .addColumn(MainFDealerFranchiseVerSchema.BIN_fAMILY, MainFDealerFranchiseVerSchema.parent_dealer_franchise_id)
                .addColumn(MainFDealerFranchiseVerSchema.BIN_fAMILY, MainFDealerFranchiseVerSchema.eff_end_datetime);

        Scan scan1 = new Scan()
                .addFamily(MainFDealerRooftopSchema.BIN_fAMILY)
                .addColumn(MainFDealerRooftopSchema.BIN_fAMILY, MainFDealerRooftopSchema.rooftop_location_id)
                .addColumn(MainFDealerRooftopSchema.BIN_fAMILY, MainFDealerRooftopSchema.rooftop_name);


        TableMapReduceUtil.initTableMapperJob(
                MainFDealerFranchiseVerSchema.TABLE_NAME,
                scan0,
                Map0.class,
                IntWritable.class,
                BytesWritable.class, job);

        TableMapReduceUtil.initTableMapperJob(
                MainFDealerRooftopSchema.TABLE_NAME,
                scan1,
                Map1.class,
                IntWritable.class,
                BytesWritable.class, job);

        TableMapReduceUtil.initTableReducerJob(
                PhmDealerFranchiseSchema.TABLE_NAME.getNameAsString(),
                ScalaJoinReduce.class,
                job);

        return (job.waitForCompletion(true)) ? 0 : 1;
    }
}