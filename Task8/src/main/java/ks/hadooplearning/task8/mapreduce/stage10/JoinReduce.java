/** В этом классе содержатся ошибки, а переписывать лень, см. класс ScalaJoinReduce вместо этого.
 */
//package ks.hadooplearning.task8.mapreduce.stage10;
//
//import ks.hadooplearning.task8.model.MainFDealerFranchiseVer;
//import ks.hadooplearning.task8.model.MainFDealerRooftop;
//import ks.hadooplearning.task8.utils.AvroDeserializer;
//import org.apache.hadoop.hbase.client.Put;
//import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
//import org.apache.hadoop.hbase.mapreduce.TableReducer;
//import org.apache.hadoop.hbase.util.Bytes;
//import org.apache.hadoop.io.BytesWritable;
//import org.apache.hadoop.io.IntWritable;
//
//import java.io.IOException;
//
//import static ks.hadooplearning.task8.schemas.PhmDealerFranchiseSchema.*;
//
///**
// * Created by ks on 10.02.15.
// */
//public class JoinReduce extends TableReducer<IntWritable, BytesWritable, ImmutableBytesWritable> {
//
//    AvroDeserializer franchDeserializer = new AvroDeserializer(MainFDealerFranchiseVer.getClassSchema());
//    AvroDeserializer rooftopDeserializer = new AvroDeserializer(MainFDealerRooftop.getClassSchema());
//
//
//    @Override
//    protected void reduce(IntWritable key, Iterable<BytesWritable> values, Context context) throws IOException, InterruptedException {
//        MainFDealerFranchiseVer franchiseVer = null;
//        MainFDealerRooftop rooftop = null;
//
//        for (BytesWritable value : values) {
//            byte[] bytes = value.getBytes();
//            byte flag = bytes[0];
//            if (flag == 0) {
//                byte[] val = Bytes.tail(bytes, bytes.length - 1);
//                franchiseVer = (MainFDealerFranchiseVer) franchDeserializer.deserialize(val);
//                for(BytesWritable value)
//            }
//            switch (flag) {
//                case (byte) 0:
//                    franchiseVer = (MainFDealerFranchiseVer) franchDeserializer.deserialize(val);
//                    break;
//                case (byte) 1:
//                    rooftop = (MainFDealerRooftop) rooftopDeserializer.deserialize(val);
//                    break;
//            }
//        }
//        if (franchiseVer != null && rooftop != null) {
//            Put put = new Put(Bytes.toBytes(franchiseVer.getDealerFranchiseId()))
//                    .add(BIN_fAMILY, dealer_franchise_id, Bytes.toBytes(franchiseVer.getDealerFranchiseId()))
//                    .add(BIN_fAMILY, dealership_id, Bytes.toBytes(franchiseVer.getRooftopLocationId()))
//                    .add(BIN_fAMILY, rooftop_name, Bytes.toBytes(rooftop.getRooftopName()));
//
//            context.write(new ImmutableBytesWritable(Bytes.toBytes(franchiseVer.getDealerFranchiseId())), put);
//        }
//    }
//}
