package ks.hadooplearning.task8.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/**
 * Created by ks on 12.02.15.
 */
public class HBaseUtils {

    private final Configuration configuration;
    private Connection connection;
    private Admin admin;

    public HBaseUtils(Configuration configuration) {
        this.configuration = configuration;
    }

    public HBaseUtils Initialize() throws IOException {
        connection = ConnectionFactory.createConnection(configuration);
        admin = connection.getAdmin();
        return this;
    }

    public Boolean existsTable(TableName tableName) throws IOException {
        Boolean result = admin.tableExists(tableName);
        return result;
    }

    public void createTable(HTableDescriptor tableDescriptor) throws IOException {
        if (admin.tableExists(tableDescriptor.getTableName())) {
            if (admin.isTableEnabled(tableDescriptor.getTableName())) {
                admin.disableTable(tableDescriptor.getTableName());
            }
            admin.deleteTable(tableDescriptor.getTableName());
        }
        admin.createTable(tableDescriptor);
    }

    public void dropTable(TableName tableName) throws IOException {
        if (admin.isTableEnabled(tableName)) {
            admin.disableTable(tableName);
        }
        admin.deleteTable(tableName);
    }

    public void close() throws IOException {
        admin.close();
        connection.close();
    }
}
