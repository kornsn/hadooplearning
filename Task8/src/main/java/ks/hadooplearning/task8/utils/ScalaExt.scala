package ks.hadooplearning.task8.utils

import ks.hadooplearning.task8.model.{MainFDealerFranchiseVer, MainFDealerRooftop}
import org.apache.hadoop.hbase.util.Bytes

/**
 * Created by ks on 13.02.15.
 */
object ScalaExts {

  private lazy val franchDeserializer = new AvroDeserializer(MainFDealerFranchiseVer.getClassSchema)
  private lazy val rooftopDeserializer = new AvroDeserializer(MainFDealerRooftop.getClassSchema)

  implicit def toBytes(x: String): Array[Byte] = Bytes.toBytes(x)

  implicit def toBytes(x: Long): Array[Byte] = Bytes.toBytes(x)

  implicit def toLong(x: Array[Byte]): Long = Bytes.toLong(x)

  implicit def toString(x: Array[Byte]): String = Bytes.toString(x)

  implicit def toInt(x: Array[Byte]): Int = Bytes.toInt(x)

  implicit def toBytes(x: Integer): Array[Byte] = Bytes.toBytes(x)

  implicit class Ext(val x: Array[Byte]) {
    def deserializeF = franchDeserializer.deserialize(x).asInstanceOf[MainFDealerFranchiseVer]

    def deserializeR = rooftopDeserializer.deserialize(x).asInstanceOf[MainFDealerRooftop]
  }

}
