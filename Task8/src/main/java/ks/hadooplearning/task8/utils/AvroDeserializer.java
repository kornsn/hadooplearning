package ks.hadooplearning.task8.utils;

import org.apache.avro.Schema;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.specific.SpecificDatumReader;

import java.io.IOException;

/**
 * Created by ks on 11.02.15.
 */
public class AvroDeserializer {

    private final Schema schema;
    private final SpecificDatumReader reader;
    private final DecoderFactory decoderFactory = DecoderFactory.get();

    public AvroDeserializer(Schema schema) {
        this.schema = schema;
        reader = new SpecificDatumReader(this.schema);
    }

    public Object deserialize(byte[] bytes) throws IOException {
        BinaryDecoder decoder = decoderFactory.binaryDecoder(bytes, null);
        Object obj = reader.read(null, decoder);
        return obj;
    }
}
