package ks.hadooplearning.task8.utils;

import org.apache.avro.Schema;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.IOException;

/**
 * Created by ks on 11.02.15.
 */
public class AvroSerializer {
    private final Schema schema;
    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
    private final DatumWriter writer;

    public AvroSerializer(Schema schema) {
        this.schema = schema;
        writer = new SpecificDatumWriter<>(schema);
    }

    public byte[] serialize(Object obj) throws IOException {
        out.reset();
        writer.write(obj, encoder);
        encoder.flush();
        out.close();
        byte[] serializedBytes = out.toByteArray();
        return serializedBytes;
    }
}
