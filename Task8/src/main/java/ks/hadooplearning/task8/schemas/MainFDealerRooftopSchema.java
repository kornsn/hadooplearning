package ks.hadooplearning.task8.schemas;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.util.Bytes;

public class MainFDealerRooftopSchema implements Schema {
    public static final TableName TABLE_NAME = TableName.valueOf("MainFDealerRooftop");
    public static final byte[] BIN_fAMILY = Bytes.toBytes("bin");
    public static final byte[] rooftop_location_id = Bytes.toBytes("rooftop_location_id");
    public static final byte[] rooftop_name = Bytes.toBytes("rooftop_name");
    public static final HTableDescriptor TABLE_DESCRIPTOR;

    static {
        TABLE_DESCRIPTOR = new HTableDescriptor(TABLE_NAME)
                .addFamily(new HColumnDescriptor(BIN_fAMILY));
    }

    @Override
    public HTableDescriptor getTableDescriptor() {
        return TABLE_DESCRIPTOR;
    }
}

