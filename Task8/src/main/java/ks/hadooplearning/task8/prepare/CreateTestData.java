package ks.hadooplearning.task8.prepare;

import ks.hadooplearning.task8.schemas.MainFDealerFranchiseVerSchema;
import ks.hadooplearning.task8.schemas.MainFDealerRooftopSchema;
import ks.hadooplearning.task8.schemas.Schema;
import ks.hadooplearning.task8.utils.HBaseUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Created by ks on 09.02.15.
 */
public class CreateTestData extends Configured implements Tool {

    public static final String[] tables = {"main.f_dealer_franchise_ver",
            "main.f_dealer_rooftop",
            "main.f_vehicle_inventory_history",
            "main.f_user_event",
            "main.f_user_event_keyvalue",
            "p_cloud.KF_DEALER_PERF_VIN_LOOKUP",
            "p_cloud.kf_agg_monthly_dealer_vin_pathing",
            "p_cloud.kf_dealer_perf_sales_detail",
            "p_cloud.kf_dealer_sales_unaccounted_inventory"};

    private static Schema[] schemas;

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(true), new CreateTestData(), args));
    }

    public static void create10(HBaseUtils utils) throws IOException {
        if (utils.existsTable(MainFDealerFranchiseVerSchema.TABLE_NAME)) {
            utils.dropTable(MainFDealerFranchiseVerSchema.TABLE_NAME);
        }
        utils.createTable(MainFDealerFranchiseVerSchema.TABLE_DESCRIPTOR);

        if (utils.existsTable(MainFDealerRooftopSchema.TABLE_NAME)) {
            utils.dropTable(MainFDealerRooftopSchema.TABLE_NAME);
        }
        utils.createTable(MainFDealerRooftopSchema.TABLE_DESCRIPTOR);

        for (int i = 0; i < 100; i++) {

        }
    }

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = getConf();

        Connection connection = ConnectionFactory.createConnection(conf);
        Admin admin = connection.getAdmin();
        for (Schema schema : schemas) {
            TableName tableName = schema.getTableDescriptor().getTableName();
            if (admin.tableExists(tableName)) {
                admin.disableTable(tableName);
                admin.deleteTable(tableName);
                System.out.println("Table " + tableName.getNameAsString() + " existed and just dropped");
            }

            admin.createTable(schema.getTableDescriptor());
            System.out.println("Table " + tableName.getNameAsString() + " created");
        }
        System.out.println("Done!");
        return 0;
    }
}
