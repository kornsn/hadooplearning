-- ---------------------------------------------------------------------------------
-- STEP 3 Re-create monthly VIN Pathing data 
-- ---------------------------------------------------------------------------------

-- 30 day inventory data for VIN pathing

create table p_cloud.temp_30_day_inventory as
(
with phm_dealer_franchise as
(	
	select df.dealer_franchise_id, df.rooftop_location_id dealership_id, rt.rooftop_name
	from main.f_dealer_franchise_ver df
	join main.f_dealer_rooftop rt
		on df.rooftop_location_id = rt.rooftop_location_id
	where df.parent_dealer_franchise_id isnull
	and df.eff_end_datetime = '9999-12-31 00:00:00'
)
SELECT 	a11.k_inventory_id as inventory_id, 
	a11.inventory_vin, 
	CASE WHEN a11.INVENTORY_TYPE IN ('USED', 'CPO') THEN 'USED' ELSE 'NEW' END INVENTORY_TYPE, 
	a11.dealer_franchise_id::integer as dealer_franchise_id,
	a11.model_year,
	a11.make,
	a11.model,
	max(a11.vehicle_trim) vehicle_trim,
	max(a11.style) style,
	max(a12.rooftop_name) as rooftop_name,
	max(a12.dealership_id) as dealership_id,
	max(a11.eff_end_datetime)::DATE inventory_max_date
FROM main.f_vehicle_inventory_history a11
JOIN phm_dealer_franchise a12 
	ON a11.DEALER_FRANCHISE_ID = a12.DEALER_FRANCHISE_ID
WHERE a11.active_flag = 'Y' 
	AND a11.make IS NOT NULL
GROUP BY 1, 2, 3, 4, 5, 6, 7
having max(eff_end_datetime) between :v_run_date::date-31 and :v_run_date::date-7
);

-- Refresh the VINS

delete from p_cloud.KF_DEALER_PERF_VIN_LOOKUP
using p_cloud.temp_30_day_inventory
where p_cloud.KF_DEALER_PERF_VIN_LOOKUP.inventory_id=p_cloud.temp_30_day_inventory.inventory_id
and p_cloud.KF_DEALER_PERF_VIN_LOOKUP.dealer_franchise_id=p_cloud.temp_30_day_inventory.dealer_franchise_id;

insert into p_cloud.KF_DEALER_PERF_VIN_LOOKUP
(select inventory_id, inventory_vin, inventory_type, rooftop_name, dealership_id, dealer_franchise_id, inventory_max_date, make, model, CONVERT_TIMEZONE('PST' ,sysdate), model_year, vehicle_trim, style
from p_cloud.temp_30_day_inventory
);

-- Purge active inventory from lookup table
create temp table temp_active_inventory as
select a11.k_inventory_id, a11.inventory_vin
from main.f_vehicle_inventory_history a11
where a11.active_flag = 'Y' 
	and eff_end_datetime = '9999-12-31 00:00:00'
;

delete from p_cloud.KF_DEALER_PERF_VIN_LOOKUP
using temp_active_inventory
where p_cloud.KF_DEALER_PERF_VIN_LOOKUP.inventory_id = temp_active_inventory.k_inventory_id
and p_cloud.KF_DEALER_PERF_VIN_LOOKUP.inventory_vin = temp_active_inventory.inventory_vin
;


-- drop temp table
drop table p_cloud.temp_30_day_inventory;



-- delete existing month of data:

delete from p_cloud.kf_agg_monthly_dealer_vin_pathing
where CLICK_MONTH_KEY >= date_part(year, (:v_run_date::date-1) ) * 100 + date_part(month, (:v_run_date::date-1));

-- refresh the VIN/ inventory pathing data for the current_month

insert into p_cloud.kf_agg_monthly_dealer_vin_pathing 
(

with phm_visitor_ddp as (
select a.f_event_guid_userid_opt as visitor_key
,a.f_date as click_date
,b.i_value::varchar(50) as dealer_location_id
,count(*) as DL_VIEWED_COUNT
from main.f_user_event a
join  main.f_user_event_keyvalue b
	ON a.k_uuid = b.k_userevent_uuid
	AND a.f_date::date = b.f_userevent_date::date
WHERE a.f_date >= date_trunc('month', :v_run_date::date-1)
and b.f_userevent_date >= date_trunc('month', :v_run_date::date-1)
AND (b.k_userevent_event_eventdata_entries_key = 'loc_id')
AND f_event_eventdata_eventtype = 'initial_state'	
AND f_event_request_pagename like '%ddp%'
and a.f_event_request_pagename NOT LIKE 'mobile%'
AND a.f_event_guid_userid_opt IS NOT NULL
AND a.f_event_guid_sessionid IS NOT NULL
AND (a.f_event_request_synpartner = 'edmunds'
	OR a.f_event_request_synpartner IS NULL ) 
AND a.f_event_browser_useragent NOT LIKE '%BingPreview%'
AND a.f_event_browser_useragent NOT LIKE '%Yahoo! Slurp%'
AND a.f_event_browser_useragent NOT LIKE '%GomezAgent%'
AND a.f_event_browser_useragent NOT LIKE '%baidu%'
AND a.f_internalip = 'N'
group by 1,2,3 )

select influence_month_key
,inventory_id
,dealer_location_id
,sum(case when influenced_visitor_count+new_influenced_visitor_count >= 1 then 1 else 0 end) influenced_visitor_count	
,sum(case when ddp_visitor_count+new_ddp_visitor_count >= 1 then 1 else 0 end) ddp_visitor_count
,vdp_visitor_count
,exposed_other_visitor_count
from (		
	select date_part(year, (click_date) ) * 100 + date_part(month, (click_date)) as INFLUENCE_MONTH_KEY,
	inventory_id as inventory_id, 
	dealer_location_id as dealer_location_id,
	sum(case when VDP_FLAG+DDP_FLAG = 2 then 1 else 0 end ) as INFLUENCED_VISITOR_COUNT,				
	sum(case when VDP_FLAG+OTHER_DDP_FLAG = 2 then 1 else 0 end ) as NEW_INFLUENCED_VISITOR_COUNT,
	sum(case when DDP_FLAG =1 then 1 else 0 end) as DDP_VISITOR_COUNT,
	sum(case when VDP_FLAG =1 then 1 else 0 end ) as VDP_VISITOR_COUNT,
	sum(case when EXPOSED_OTHER_FLAG =1 then 1 else 0  end) as EXPOSED_OTHER_VISITOR_COUNT,
	sum(case when OTHER_DDP_FLAG = 1 then 1 else 0 end) as NEW_DDP_VISITOR_COUNT
	from (
		select 	f_event_guid_userid_opt as visitor_key, 
		f_date as click_date, 
		b.i_value::VARCHAR(75) as inventory_id,
		c.i_value::varchar(50) as dealer_location_id,
		max(case when f_event_request_pagename in ('new_model_car_inventory_ddp_listings', 'new_make_car_inventory_ddp_listings') 
			then 1 else 0 end) as DDP_FLAG,
		max(case when f_event_request_pagename = 'new_model_car_inventory_vin_detail' 
			then 1 else 0 end) as VDP_FLAG,
		max( case when 	f_event_request_pagename not in ('new_model_car_inventory_vin_detail', 'new_model_car_inventory_ddp_listings', 'new_make_car_inventory_ddp_listings') 
			then 1 else 0 end) as EXPOSED_OTHER_FLAG,
		max(case when DL_VIEWED_COUNT is not null then 1 else 0 end) as OTHER_DDP_FLAG,
		count(*) as LIST_COUNT
		from main.f_user_event a
		join  main.f_user_event_keyvalue b
			ON a.k_uuid = b.k_userevent_uuid
			AND a.f_date::date = b.f_userevent_date::date
		join  main.f_user_event_keyvalue c
			ON a.k_uuid = c.k_userevent_uuid
			AND a.f_date::date = c.f_userevent_date::date
			AND b.k_userevent_uuid = c.k_userevent_uuid
			AND substring(b.k_userevent_event_eventdata_entries_key,8,4) = substring(c.k_userevent_event_eventdata_entries_key,8,4)
		left join phm_visitor_ddp d
			on a.f_event_guid_userid_opt = d.visitor_key
			and a.f_date = d.click_date
			and c.i_value::varchar(50) = d.dealer_location_id
		WHERE a.f_date >= date_trunc('month', :v_run_date::date-1)
		and b.f_userevent_date >= date_trunc('month', :v_run_date::date-1)
		and c.f_userevent_date >= date_trunc('month', :v_run_date::date-1)
		AND (b.k_userevent_event_eventdata_entries_key LIKE 'inv_id%')
		AND (c.k_userevent_event_eventdata_entries_key LIKE 'loc_id%')
		AND f_event_eventdata_eventtype = 'list'
		and a.f_event_request_pagename NOT LIKE 'mobile%'
		AND a.f_event_guid_userid_opt IS NOT NULL
		AND a.f_event_guid_sessionid IS NOT NULL
		AND (
			a.f_event_request_synpartner = 'edmunds'
			OR a.f_event_request_synpartner IS NULL
			) 
		AND a.f_event_browser_useragent NOT LIKE '%BingPreview%'
		AND a.f_event_browser_useragent NOT LIKE '%Yahoo! Slurp%'
		AND a.f_event_browser_useragent NOT LIKE '%GomezAgent%'
		AND a.f_event_browser_useragent NOT LIKE '%baidu%'
		AND a.f_internalip = 'N'
		group by 1,2,3,4) as phm
	group by 1,2,3) a
group by 1,2,3,6,7 );



-- truncate the unaccounted inventory data

truncate table p_cloud.kf_dealer_sales_unaccounted_inventory;

-- insert the latest unaccounted inventory

insert into p_cloud.kf_dealer_sales_unaccounted_inventory 
(
select 	inventory_id, 
		inventory_vin, 
		dealer_rooftop_name, 
		dealer_location_id::varchar(50), 
		dealer_franchise_id, 
		max(inventory_max_date) as projected_Sale_date,
		date_part(year, (max(inventory_max_date)) ) * 100 
			+ date_part(month, (max(inventory_max_date))) AS  PROJECTED_SALE_MONTH,
		date_part(year, (max(inventory_max_date)-30) ) * 100 
			+ date_part(month, (max(inventory_max_date)-30)) AS  PROJECTED_INFLUENCE_MONTH,
		make,
		model,
		model_year,
		vehicle_trim,
		style
from 
	(
	select distinct inventory_id, inventory_vin,
		first_value(dealer_rooftop_name ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as dealer_rooftop_name, 
		first_value(dealer_location_id  ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as dealer_location_id,  
		first_value(dealer_franchise_id  ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as dealer_franchise_id,   
		first_value(inventory_max_date  ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as inventory_max_date,
		first_value(make ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as make,
		first_value(model ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as model,
		first_value(model_year ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as model_year,
		first_value(vehicle_trim  ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as vehicle_trim,
		first_value(style ignore nulls) over(partition by  inventory_id, inventory_vin  order by inventory_max_date desc rows between unbounded preceding and unbounded following) as style
	from p_cloud.KF_DEALER_PERF_VIN_LOOKUP
	where inventory_type = 'NEW'
	) A
left join p_cloud.kf_dealer_perf_sales_detail B
on A.inventory_vin = B.f_vehicle_vin
where B.f_vehicle_vin is NULL
and date_part(year, (inventory_max_date) ) * 100 + date_part(month, (inventory_max_date)) 
	>= date_part(year, dateadd(month, - 19, :v_run_date::date - 1)) * 100 
		+ date_part(month, dateadd(month, - 19, :v_run_date::date - 1))
group by 1,2,3,4,5,9,10,11,12,13
);
