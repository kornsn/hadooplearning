package ks.hadooplearning.task3;

import ks.hadooplearning.task3.data.TestData;
import ks.hadooplearning.task3.helpers.ConcreteParser;
import ks.hadooplearning.task3.helpers.ParseResult;
import ks.hadooplearning.task3.helpers.Parser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConcreteParserTest {

    private String testData;
    private Parser parser;
    private ParseResult result;

    @Before
    public void setUp() throws Exception {
        testData = TestData.input[0];
        parser = new ConcreteParser();
        result = parser.parse(testData);
    }

    @Test
    public void testParse() throws Exception {
        parser.parse(testData);
    }

    @Test
    public void testGetLocation() throws Exception {
        Assert.assertEquals("Алтайский край", result.getMfc().getLocation());
    }

    @Test
    public void testGetLocation2() throws Exception {
        Assert.assertEquals("Благовещенский муниципальный район", result.getMfc().getLocation2());
    }

    @Test
    public void testGetWindowsCount() throws Exception {
        Assert.assertEquals((Integer) 7, result.getMfc().getWindowsCount());
    }

    @Test
    public void testGetWebSite() throws Exception {
        Assert.assertEquals("http://mfc22.ru/", result.getMfc().getWebsite());
    }
}