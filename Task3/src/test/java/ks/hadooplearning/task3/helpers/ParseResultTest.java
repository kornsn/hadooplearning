package ks.hadooplearning.task3.helpers;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ValidationException;

public class ParseResultTest extends TestCase {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    /**
     * Проверяется, что при попытке создать ParseResult с Mfc равным null вызывается ValidationException
     */
    @Test(expected = ValidationException.class)
    public void testCreateNullMfc() throws Exception {
        ParseResult.create(null);
    }

    @Test
    public void testCreate() throws Exception {

    }

    @Test
    public void testIsSuccess() throws Exception {

    }

    @Test
    public void testGetMfc() throws Exception {

    }
}