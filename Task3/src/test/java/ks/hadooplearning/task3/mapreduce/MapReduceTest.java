/**
 * Commented cause mrunit does not support multipleOutput yet, so tests end with error.
 *
 *
package ks.hadooplearning.task3.mapreduce;

import ks.hadooplearning.task3.data.TestData;
import ks.hadooplearning.task3.helpers.Avro2Bytes;
import ks.hadooplearning.task3.model.MFC;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class MapReduceTest {
    MapDriver<LongWritable, Text, Text, BytesWritable> mapDriver;
    ReduceDriver<Text, BytesWritable, Text, Writable> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, BytesWritable, Text, Writable> mapReduceDriver;
    Avro2Bytes avro2Bytes;
    byte[][] values;

    @Before
    public void setUp() throws IOException {
        Map mapper = new Map();
        Reduce reduce = new Reduce();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reduce);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reduce);
        avro2Bytes = new Avro2Bytes(MFC.getClassSchema());
        values = new byte[][]{
                avro2Bytes.serialize(
                        new MFC(
                                "Алтайский край",
                                "Благовещенский муниципальный район",
                                7,
                                "http://mfc22.ru/")),
                avro2Bytes.serialize(
                        new MFC(
                                "Алтайский край",
                                "Городской округ Барнаул",
                                41,
                                "http://mfc22.ru/"))};
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver
                .withInput(new LongWritable(1), new Text(TestData.input[0]))
                .withOutput(
                        new Text("Алтайский край"),
                        new BytesWritable(values[0]))
                .runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<BytesWritable> values1 = Arrays.asList(new BytesWritable(values[0]), new BytesWritable(values[1]));
        reduceDriver
                .withInput(new Text("Алтайский край"), values1)
                .withMultiOutput("max", new Text("Алтайский край"), new IntWritable(41))
                .withMultiOutput("avg", new Text("Алтайский край"), new FloatWritable(24))
                .withMultiOutput("sum", new Text("Алтайский край"), new IntWritable(48))
                .runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(new LongWritable(1), new Text(TestData.input[0]))
                .withMultiOutput("max", new Text("Алтайский край"), new IntWritable(41))
                .withMultiOutput("avg", new Text("Алтайский край"), new FloatWritable(24))
                .withMultiOutput("sum", new Text("Алтайский край"), new IntWritable(48))
                .runTest();
    }

    }

 */