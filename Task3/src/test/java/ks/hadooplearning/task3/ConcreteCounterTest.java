package ks.hadooplearning.task3;

import junit.framework.TestCase;
import ks.hadooplearning.task3.helpers.ConcreteCounter;
import ks.hadooplearning.task3.helpers.CountResult;
import ks.hadooplearning.task3.helpers.Counter;
import org.junit.Assert;

import java.util.Arrays;

public class ConcreteCounterTest extends TestCase {

    private Iterable<Integer> testData;
    private Counter counter;
    private CountResult result;

    public void setUp() throws Exception {
        super.setUp();

        counter = new ConcreteCounter();
        testData = Arrays.asList(1, 49, 22);

        result = counter.count(testData);
    }

    public void testGetMax() throws Exception {
        Assert.assertEquals(49, result.getMax());
    }

    public void testGetAvg() throws Exception {
        Assert.assertEquals(24, result.getAvg(), 1e-3);
    }

    public void testGetSum() throws Exception {
        Assert.assertEquals(72, result.getSum());
    }

    public void testCount() throws Exception {
        counter.count(testData);
    }
}