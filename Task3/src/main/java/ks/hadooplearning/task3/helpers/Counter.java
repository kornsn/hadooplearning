package ks.hadooplearning.task3.helpers;

/**
 * Created by ks on 1/13/15.
 */
public interface Counter {
    CountResult count(Iterable<Integer> values);
}
