package ks.hadooplearning.task3.helpers;

/**
 * Created by ks on 1/13/15.
 */
public class ConcreteCounter implements Counter {

    @Override
    public CountResult count(Iterable<Integer> values) {
        int sum = 0;
        int valueCount = 0;
        int max = 0;
        for (Integer v : values) {
            valueCount++;
            sum += v;
            if (v > max) max = v;
        }
        float avg = valueCount > 0 ? (float) sum / valueCount : 0;
        return new CountResult(max, avg, sum);
    }
}
