package ks.hadooplearning.task3.helpers;

import ks.hadooplearning.task3.model.MFC;

import javax.validation.constraints.NotNull;

/**
 * Результат парсинга.
 */
public class ParseResult {

    /**
     * Получает экземпляр ParseResult, показывающий, что парсинг прошел неуспешно.
     */
    public static final ParseResult NOT_SUCCESS_RESULT = new ParseResult(false, null);

    private final boolean isSuccess;
    private final MFC mfc;


    private ParseResult(Boolean isSuccess, MFC mfc) {
        this.isSuccess = isSuccess;
        this.mfc = mfc;
    }

    /**
     * Создает новый экземпляр класса ParseResult с значением isSuccess == true.
     * Если требуется отрицательный результат, используй поле NOT_SUCCESS_RESULT.
     *
     * @param mfc
     * @return
     */
    public static ParseResult create(@NotNull MFC mfc) {
        return new ParseResult(true, mfc);
    }

    /**
     * Получает значение, показывающее, успешно ли прошел парсинг.
     */
    public boolean isSuccess() {
        return isSuccess;
    }

    /**
     * Получает результат парсинга -- экземпляр класса MFC.
     *
     * @return
     */
    public MFC getMfc() {
        return mfc;
    }
}
