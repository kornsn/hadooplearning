package ks.hadooplearning.task3.helpers;

import org.apache.avro.Schema;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.IOException;

/**
 * Created by ks on 30.01.15.
 */
public class Avro2Bytes {

    Schema schema;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
    DatumWriter writer = new SpecificDatumWriter(schema);

    SpecificDatumReader reader;

    public Avro2Bytes(Schema schema) {
        this.schema = schema;
        out = new ByteArrayOutputStream();
        encoder = EncoderFactory.get().binaryEncoder(out, null);
        writer = new SpecificDatumWriter<>(schema);
        reader = new SpecificDatumReader(schema);
    }

    public byte[] serialize(Object obj) throws IOException {
        out.reset();
        writer.write(obj, encoder);
        encoder.flush();
        out.close();
        byte[] serializedBytes = out.toByteArray();
        return serializedBytes;
    }

    public Object deserialize(byte[] bytes) throws IOException {
        Decoder decoder = DecoderFactory.get().binaryDecoder(bytes, null);
        Object obj = reader.read(null, decoder);
        return obj;
    }
}
