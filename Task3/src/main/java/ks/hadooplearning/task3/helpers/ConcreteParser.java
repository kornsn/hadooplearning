package ks.hadooplearning.task3.helpers;

import ks.hadooplearning.task3.model.MFC;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser
 * Created by ks on 1/13/15.
 */
public class ConcreteParser implements Parser {

    /**
     * Pattern for parsing
     */
    private static final Pattern pattern = Pattern.compile
            ("^\\s*(\\d+),\"(.*)\",\"(.*)\",\"(.*)\",\"(.*)\",(\\d+),\"(.*)\",\"(.*)\",\"(.*)\",\"(.*)\",(.*),(\\d*\\.?\\d*),(\\d*\\.?\\d*)\\s*$");

    /**
     * Parse string
     *
     * @param str
     * @return
     */
    @Override
    public ParseResult parse(String str) {
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            try {
                String location = matcher.group(2);
                String location2 = matcher.group(3);
                int windowsCount = Integer.parseInt(matcher.group(6));
                String webSite = matcher.group(8);
                MFC mfc = MFC.newBuilder()
                        .setLocation(location)
                        .setLocation2(location2)
                        .setWebsite(webSite)
                        .setWindowsCount(windowsCount)
                        .build();
                return ParseResult.create(mfc);
            } catch (Exception ex) {
                return ParseResult.NOT_SUCCESS_RESULT;
            }
        } else return ParseResult.NOT_SUCCESS_RESULT;
    }
}
