package ks.hadooplearning.task3.helpers;

/**
 * Counter result.
 */
public class CountResult {
    private final int max;
    private final float avg;
    private final int sum;


    /**
     * @param max maximum
     * @param avg average
     * @param sum sum
     */
    public CountResult(int max, float avg, int sum) {
        this.max = max;
        this.avg = avg;
        this.sum = sum;
    }

    /**
     * Gets maximum.
     *
     * @return
     */
    public int getMax() {
        return max;
    }

    /**
     * Gets average.
     *
     * @return
     */
    public float getAvg() {
        return avg;
    }

    /**
     * Gets sum.
     *
     * @return
     */
    public int getSum() {
        return sum;
    }
}
