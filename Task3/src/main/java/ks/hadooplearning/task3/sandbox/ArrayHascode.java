package ks.hadooplearning.task3.sandbox;

/**
 * Created by ks on 04.02.15.
 */
public class ArrayHascode {
    public static void main(String[] args) {
        Byte[] a = {1, 2, 3};
        Byte[] b = {1, 2, 3};

        System.out.println(a.hashCode());
        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
    }
}
