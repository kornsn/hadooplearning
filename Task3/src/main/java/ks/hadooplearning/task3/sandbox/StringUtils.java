package ks.hadooplearning.task3.sandbox;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * Created by ks on 04.02.15.
 */
public class StringUtils {
    public static void main(String[] args) {
        System.out.println(isBlank("\t"));
        System.out.println(isBlank(null));
    }
}
