package ks.hadooplearning.task3.importer;

import ks.hadooplearning.task3.config.AppContext;
import ks.hadooplearning.task3.helpers.ParseResult;
import ks.hadooplearning.task3.helpers.Parser;
import ks.hadooplearning.task3.model.MFC;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

import static ks.hadooplearning.task3.hbase.InputSchema.*;

/**
 * Created by ks on 25.01.15.
 */
public class Map<K, V> extends Mapper<LongWritable, Text, K, V> {

    private Parser parser;
    private Table table;
    private Connection connection;

    @Override
    protected void setup(Context context) throws IOException {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppContext.class);
        parser = applicationContext.getBean(Parser.class);
        Configuration configuration = context.getConfiguration();
        connection = ConnectionFactory.createConnection(configuration);
        this.table = connection.getTable(TABLE_NAME);
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        ParseResult parseResult = parser.parse(value.toString());
        if (parseResult.isSuccess()) {
            MFC mfc = parseResult.getMfc();
            byte[] rowKey = Bytes.add(Bytes.toBytes(mfc.getLocation()), Bytes.toBytes(mfc.getLocation2()));
            Put p = new Put(rowKey);
            p.add(DATA_COLUMN_FAMILY, LOCATION_QUALIFIER, Bytes.toBytes(mfc.getLocation()));
            p.add(DATA_COLUMN_FAMILY, LOCATION2_QUALIFIER, Bytes.toBytes(mfc.getLocation2()));
            p.add(DATA_COLUMN_FAMILY, WEBSITE_QUALIFIER, Bytes.toBytes(mfc.getWebsite()));
            p.add(DATA_COLUMN_FAMILY, WINDOWS_COUNT_QUALIFIER, Bytes.toBytes(mfc.getWindowsCount()));
            table.put(p);
        }
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        table.close();
        connection.close();
    }
}
