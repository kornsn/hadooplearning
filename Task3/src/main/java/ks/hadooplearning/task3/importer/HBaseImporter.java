package ks.hadooplearning.task3.importer;

import ks.hadooplearning.task3.hbase.HBaseUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import static ks.hadooplearning.task3.hbase.InputSchema.TABLE_DESCRIPTOR;

/**
 * Imports data from csv-file to hbase table.
 */
public class HBaseImporter extends Configured implements Tool {

    public static final String JOB_NAME = "Task3.ImportData";


    /**
     * Entry point
     *
     * @param args inputDirectory outputTableName
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new HBaseImporter(), args));
    }

    /**
     * Run job.commit
     *
     * @param args command-line args.
     * @return 0 in success otherwise 1.
     * @throws Exception
     */
    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Usage: " + getClass().getName() + " <input>");
            return 1;
        }

        Configuration conf = getConf();

        System.out.println(conf.get("zookeeper.znode.parent"));

        // workaround for error "client.ZooKeeperRegistry: ClusterId read in ZooKeeper is null"
        // see http://2scompliment.wordpress.com/2013/12/11/running-hbase-java-applications-on-hortonworks-hadoop-sandbox-2-x-with-yarn/
        conf.set("zookeeper.znode.parent", "/hbase-unsecure");

        Job job = Job.getInstance(conf, JOB_NAME);

        FileInputFormat.addInputPath(job, new Path(args[0]));

        job.setJarByClass(getClass());
        job.setMapperClass(Map.class);
        job.setNumReduceTasks(0);
        job.setOutputFormatClass(NullOutputFormat.class);

        HBaseUtils.prepareOutTable(conf, TABLE_DESCRIPTOR);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
