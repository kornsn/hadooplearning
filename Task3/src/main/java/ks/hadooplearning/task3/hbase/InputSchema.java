package ks.hadooplearning.task3.hbase;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Created by ks on 04.02.15.
 */
public class InputSchema implements SimpleSchema {

    public static final TableName TABLE_NAME = TableName.valueOf("task3in");
    public static final byte[] DATA_COLUMN_FAMILY = Bytes.toBytes("data");
    public static final byte[] LOCATION_QUALIFIER = Bytes.toBytes("location");
    public static final byte[] LOCATION2_QUALIFIER = Bytes.toBytes("location2");
    public static final byte[] WEBSITE_QUALIFIER = Bytes.toBytes("website");
    public static final byte[] WINDOWS_COUNT_QUALIFIER = Bytes.toBytes("windows_count");
    public static final HTableDescriptor TABLE_DESCRIPTOR;

    static {
        TABLE_DESCRIPTOR = new HTableDescriptor(TABLE_NAME);
        TABLE_DESCRIPTOR.addFamily(new HColumnDescriptor(DATA_COLUMN_FAMILY));
    }

    @Override
    public HTableDescriptor getDescriptor() {
        return TABLE_DESCRIPTOR;
    }
}
