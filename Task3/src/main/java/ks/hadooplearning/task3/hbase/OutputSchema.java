package ks.hadooplearning.task3.hbase;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Created by ks on 04.02.15.
 */
public class OutputSchema implements SimpleSchema {

    public static final TableName TABLE_NAME = TableName.valueOf("task3out");
    public static final byte[] DATA_COLUMN_FAMILY = Bytes.toBytes("data");
    public static final byte[] MAX_QUALIFIER = Bytes.toBytes("max");
    public static final byte[] AVG_QUALIFIER = Bytes.toBytes("avg");
    public static final byte[] SUM_QUALIFIER = Bytes.toBytes("sum");
    public static final HTableDescriptor TABLE_DESCRIPTOR;

    static {
        TABLE_DESCRIPTOR = new HTableDescriptor(TABLE_NAME);
        TABLE_DESCRIPTOR.addFamily(new HColumnDescriptor(DATA_COLUMN_FAMILY));
    }

    @Override
    public HTableDescriptor getDescriptor() {
        return TABLE_DESCRIPTOR;
    }
}
