package ks.hadooplearning.task3.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/**
 * Created by ks on 04.02.15.
 */
public class HBaseUtils {

    /**
     * Prepare table for data inserting.
     *
     * @param configuration
     * @param tableDescriptor
     * @throws IOException
     */
    public static void prepareOutTable(Configuration configuration, HTableDescriptor tableDescriptor) throws IOException {
        Connection connection = ConnectionFactory.createConnection(configuration);
        Admin admin = connection.getAdmin();

        if (admin.tableExists(tableDescriptor.getTableName())) {
            if (admin.isTableEnabled(tableDescriptor.getTableName())) {
                admin.disableTable(tableDescriptor.getTableName());
            }
            admin.deleteTable(tableDescriptor.getTableName());
        }

        admin.createTable(tableDescriptor);

        admin.close();
        connection.close();
    }

    public static Boolean checkIfExists(Configuration configuration, TableName tableName) throws IOException {
        Connection connection = ConnectionFactory.createConnection(configuration);
        Admin admin = connection.getAdmin();

        Boolean result = admin.tableExists(tableName);

        admin.close();
        connection.close();
        return result;
    }
}