package ks.hadooplearning.task3.hbase;

import org.apache.hadoop.hbase.HTableDescriptor;

/**
 * Created by ks on 04.02.15.
 */
public interface SimpleSchema {
    HTableDescriptor getDescriptor();
}
