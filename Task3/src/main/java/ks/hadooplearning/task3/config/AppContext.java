package ks.hadooplearning.task3.config;

import ks.hadooplearning.task3.helpers.ConcreteCounter;
import ks.hadooplearning.task3.helpers.ConcreteParser;
import ks.hadooplearning.task3.helpers.Counter;
import ks.hadooplearning.task3.helpers.Parser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppContext {

    @Bean
    public Parser parser() {
        return new ConcreteParser();
    }

    @Bean
    public Counter counter() {
        return new ConcreteCounter();
    }

}
