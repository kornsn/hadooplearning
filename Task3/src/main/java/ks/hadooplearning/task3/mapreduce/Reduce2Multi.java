package ks.hadooplearning.task3.mapreduce;

import com.google.common.base.Function;
import ks.hadooplearning.task3.config.AppContext;
import ks.hadooplearning.task3.helpers.Avro2Bytes;
import ks.hadooplearning.task3.helpers.CountResult;
import ks.hadooplearning.task3.helpers.Counter;
import ks.hadooplearning.task3.model.MFC;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

import static com.google.common.collect.Iterables.transform;

/**
 * Reducer class
 */
public class Reduce2Multi extends Reducer<Text, BytesWritable, Text, Writable> {

    private Counter counter;
    private MultipleOutputs<Text, Writable> multipleOutputs;
    private Avro2Bytes avro2Bytes;

    /**
     * Set outputs
     *
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppContext.class);
        counter = applicationContext.getBean(Counter.class);
        avro2Bytes = new Avro2Bytes(MFC.SCHEMA$);

        multipleOutputs = new MultipleOutputs<>(context);
    }

    /**
     * Close outputs
     *
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        multipleOutputs.close();
    }

    /**
     * Reduce
     *
     * @param key
     * @param values
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void reduce(Text key, Iterable<BytesWritable> values, final Context context)
            throws IOException, InterruptedException {

        // deserialize avro's
        Iterable<MFC> mfcs = transform(values, new Function<BytesWritable, MFC>() {
            @Override
            public MFC apply(BytesWritable value) {
                MFC mfc = null;
                try {
                    mfc = (MFC) avro2Bytes.deserialize(value.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                    context.getCounter(Counters.ERRORS).increment(1);
                }
                return mfc;
            }
        });

        // count windows
        CountResult countResult = counter.count(transform(mfcs, new Function<MFC, Integer>() {
            @Override
            public Integer apply(MFC mfc) {
                return mfc.getWindowsCount();
            }
        }));

        // write result
        multipleOutputs.write("max", key, countResult.getMax());
        multipleOutputs.write("avg", key, countResult.getAvg());
        multipleOutputs.write("sum", key, countResult.getSum());
    }


}


