package ks.hadooplearning.task3.mapreduce;

import com.google.common.base.Function;
import ks.hadooplearning.task3.config.AppContext;
import ks.hadooplearning.task3.helpers.Avro2Bytes;
import ks.hadooplearning.task3.helpers.CountResult;
import ks.hadooplearning.task3.helpers.Counter;
import ks.hadooplearning.task3.model.MFC;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

import static com.google.common.collect.Iterables.transform;
import static ks.hadooplearning.task3.hbase.OutputSchema.*;

/**
 * Created by ks on 04.02.15.
 */
public class Reduce2HBase extends TableReducer<Text, BytesWritable, ImmutableBytesWritable> {
    private Counter counter;
    private Avro2Bytes avro2Bytes;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppContext.class);
        counter = applicationContext.getBean(Counter.class);
        avro2Bytes = new Avro2Bytes(MFC.SCHEMA$);
    }

    @Override
    protected void reduce(Text key, Iterable<BytesWritable> values, final Context context) throws IOException, InterruptedException {
        // deserialize avro's
        Iterable<MFC> mfcs = transform(values, new Function<BytesWritable, MFC>() {
            @Override
            public MFC apply(BytesWritable value) {
                MFC mfc = null;
                try {
                    mfc = (MFC) avro2Bytes.deserialize(value.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                    context.getCounter(Counters.ERRORS).increment(1);
                }
                return mfc;
            }
        });

        // count windows
        CountResult countResult = counter.count(transform(mfcs, new Function<MFC, Integer>() {
            @Override
            public Integer apply(MFC mfc) {
                return mfc.getWindowsCount();
            }
        }));

        // write result
        Put put = new Put(key.getBytes());
        put.add(DATA_COLUMN_FAMILY, MAX_QUALIFIER, Bytes.toBytes(countResult.getMax()));
        put.add(DATA_COLUMN_FAMILY, AVG_QUALIFIER, Bytes.toBytes(countResult.getAvg()));
        put.add(DATA_COLUMN_FAMILY, SUM_QUALIFIER, Bytes.toBytes(countResult.getSum()));

        context.write(new ImmutableBytesWritable(key.getBytes()), put);
    }
}
