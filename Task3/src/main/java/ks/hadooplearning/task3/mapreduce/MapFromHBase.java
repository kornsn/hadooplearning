package ks.hadooplearning.task3.mapreduce;

import ks.hadooplearning.task3.helpers.Avro2Bytes;
import ks.hadooplearning.task3.model.MFC;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;

import java.io.IOException;

import static ks.hadooplearning.task3.hbase.InputSchema.*;

/**
 * Created by ks on 04.02.15.
 */
public class MapFromHBase extends TableMapper<Text, BytesWritable> {
    private Avro2Bytes avro2Bytes = new Avro2Bytes(MFC.getClassSchema());
    private Text keyOut = new Text();
    private BytesWritable valueOut = new BytesWritable();

    @Override
    protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
        String outKey = Bytes.toString(value.getRow());
        String location = Bytes.toString(value.getValue(DATA_COLUMN_FAMILY, LOCATION_QUALIFIER));
        String location2 = Bytes.toString(value.getValue(DATA_COLUMN_FAMILY, LOCATION2_QUALIFIER));
        String website = Bytes.toString(value.getValue(DATA_COLUMN_FAMILY, WEBSITE_QUALIFIER));
        Integer wincount = Bytes.toInt(value.getValue(DATA_COLUMN_FAMILY, WINDOWS_COUNT_QUALIFIER));
        MFC mfc = MFC.newBuilder()
                .setLocation(location)
                .setLocation2(location2)
                .setWebsite(website)
                .setWindowsCount(wincount)
                .build();
        byte[] serialized = avro2Bytes.serialize(mfc);
        keyOut.set(outKey);
        valueOut.set(serialized, 0, serialized.length);
        context.write(keyOut, valueOut);
        context.getCounter(Counters.PROCESSED).increment(1);
    }
}
