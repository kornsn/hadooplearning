package ks.hadooplearning.task3.mapreduce;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Driver class
 */
public class Driver extends Configured implements Tool {

    public static final String JOB_NAME = "Task 3";

    /**
     * Entry point
     *
     * @param args Command line args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new Driver(), args));
    }


    @Override
    public int run(String[] args) throws Exception {
        if (args.length < 2) {
            System.err.println("Usage: Driver <in> <out>");
            return 1;
        }

        Path input = new Path(args[0]);
        Path output = new Path(args[1]);

        Configuration conf = getConf();

        FileSystem.newInstance(conf).delete(output, true);
        Job job = Job.getInstance(conf, JOB_NAME);
        job.setJarByClass(getClass());
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce2Multi.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(BytesWritable.class);

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        MultipleOutputs.addNamedOutput(job, "max", TextOutputFormat.class, Text.class, IntWritable.class);
        MultipleOutputs.addNamedOutput(job, "avg", TextOutputFormat.class, Text.class, FloatWritable.class);
        MultipleOutputs.addNamedOutput(job, "sum", TextOutputFormat.class, Text.class, IntWritable.class);

        return (job.waitForCompletion(true)) ? 0 : 1;
    }
}
