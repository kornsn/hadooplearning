package ks.hadooplearning.task3.mapreduce;

import ks.hadooplearning.task3.hbase.HBaseUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import static ks.hadooplearning.task3.hbase.OutputSchema.TABLE_DESCRIPTOR;
import static ks.hadooplearning.task3.hbase.OutputSchema.TABLE_NAME;

/**
 * Driver class
 */
public class DriverHdfs2HBase extends Configured implements Tool {

    public static final String JOB_NAME = "Task 3 Hdfs2HBase";

    /**
     * Entry point
     *
     * @param args Command line args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new DriverHdfs2HBase(), args));
    }

    /**
     * Run job.
     *
     * @param args
     * @return
     * @throws Exception
     */
    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Usage: " + getClass().getName() + " <in>");
            return 1;
        }

        Path input = new Path(args[0]);

        Configuration conf = getConf();

        // workaround for error "client.ZooKeeperRegistry: ClusterId read in ZooKeeper is null"
        // see http://2scompliment.wordpress.com/2013/12/11/running-hbase-java-applications-on-hortonworks-hadoop-sandbox-2-x-with-yarn/
        conf.set("zookeeper.znode.parent", "/hbase-unsecure");

        Job job = Job.getInstance(conf, JOB_NAME);
        job.setJarByClass(getClass());
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce2HBase.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(BytesWritable.class);

        FileInputFormat.addInputPath(job, input);

        HBaseUtils.prepareOutTable(conf, TABLE_DESCRIPTOR);

        TableMapReduceUtil.initTableReducerJob(
                TABLE_NAME.getNameAsString(),
                Reduce2HBase.class, job);

        return (job.waitForCompletion(true)) ? 0 : 1;
    }
}
