package ks.hadooplearning.task3.mapreduce;

/**
 * Contains map-reduce counter names.
 */
public enum Counters {
    /**
     * Contains exceptions count.
     */
    ERRORS,

    /**
     * Contains processed values count.
     */
    PROCESSED
}
