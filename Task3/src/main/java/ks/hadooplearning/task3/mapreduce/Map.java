package ks.hadooplearning.task3.mapreduce;

import ks.hadooplearning.task3.config.AppContext;
import ks.hadooplearning.task3.helpers.Avro2Bytes;
import ks.hadooplearning.task3.helpers.ParseResult;
import ks.hadooplearning.task3.helpers.Parser;
import ks.hadooplearning.task3.model.MFC;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * Mapper class
 */
public class Map extends Mapper<LongWritable, Text, Text, BytesWritable> {
    private final Text key = new Text();
    private Avro2Bytes avro2Bytes;
    private Parser parser;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppContext.class);
        parser = applicationContext.getBean(Parser.class);
        avro2Bytes = new Avro2Bytes(MFC.SCHEMA$);
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        // parse and filter valid rows and non-empty website
        ParseResult parseResult = parser.parse(value.toString());
        if (parseResult.isSuccess()) {
            MFC mfc = parseResult.getMfc();
            if (StringUtils.isNotBlank(String.valueOf(mfc.getWebsite()))) {
                this.key.set(String.valueOf(mfc.getLocation()));
                context.write(this.key, new BytesWritable(avro2Bytes.serialize(mfc)));
            }
        }

        context.getCounter(Counters.PROCESSED).increment(1);
    }


}
