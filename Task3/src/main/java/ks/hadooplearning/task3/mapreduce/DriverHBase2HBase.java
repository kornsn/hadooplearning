package ks.hadooplearning.task3.mapreduce;

import ks.hadooplearning.task3.hbase.HBaseUtils;
import ks.hadooplearning.task3.hbase.InputSchema;
import ks.hadooplearning.task3.hbase.OutputSchema;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Created by ks on 04.02.15.
 */
public class DriverHBase2HBase extends Configured implements Tool {
    public static final String JOB_NAME = "Task 3 HBase2HBase";

    /**
     * Entry point
     *
     * @param args Command line args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new DriverHBase2HBase(), args));
    }

    /**
     * Run job.
     *
     * @param args
     * @return
     * @throws Exception
     */
    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 0) {
            System.err.println("Usage: " + getClass().getName());
            return 1;
        }

        Configuration conf = getConf();

        // workaround for error "client.ZooKeeperRegistry: ClusterId read in ZooKeeper is null"
        // see http://2scompliment.wordpress.com/2013/12/11/running-hbase-java-applications-on-hortonworks-hadoop-sandbox-2-x-with-yarn/
        conf.set("zookeeper.znode.parent", "/hbase-unsecure");

        Job job = Job.getInstance(conf, JOB_NAME);
        job.setJarByClass(getClass());
        job.setMapperClass(MapFromHBase.class);
        job.setReducerClass(Reduce2HBase.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(BytesWritable.class);

        if (!HBaseUtils.checkIfExists(conf, InputSchema.TABLE_NAME)) {
            System.err.println("Input table does not exists");
            return 1;
        }
        HBaseUtils.prepareOutTable(conf, OutputSchema.TABLE_DESCRIPTOR);

        Scan inputScan = new Scan()
                .addFamily(InputSchema.DATA_COLUMN_FAMILY)
                .addColumn(InputSchema.DATA_COLUMN_FAMILY, InputSchema.LOCATION_QUALIFIER)
                .addColumn(InputSchema.DATA_COLUMN_FAMILY, InputSchema.LOCATION2_QUALIFIER)
                .addColumn(InputSchema.DATA_COLUMN_FAMILY, InputSchema.WEBSITE_QUALIFIER)
                .addColumn(InputSchema.DATA_COLUMN_FAMILY, InputSchema.WINDOWS_COUNT_QUALIFIER);

        TableMapReduceUtil.initTableMapperJob(
                InputSchema.TABLE_NAME.getNameAsString(),
                inputScan,
                MapFromHBase.class,
                Text.class,
                BytesWritable.class, job);

        TableMapReduceUtil.initTableReducerJob(
                OutputSchema.TABLE_NAME.getNameAsString(),
                Reduce2HBase.class, job);

        return (job.waitForCompletion(true)) ? 0 : 1;
    }
}
