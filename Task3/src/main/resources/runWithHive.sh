#!/bin/bash

# check arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: `basename $0` <inDir> <outFile>";
    exit 1;
fi

echo "hello from '$1'";

## create table
#hive -f "hive/create.sql" &&
#
## load data
#hive -e 'load data inpath $1 into table mfc' &&
#
## filter and calculate
#hive -f "hive/calculate.sql" > $2 &&
#echo "Done.";

hive -f hive/exec.sql -hiveconf INPUT=$1 > $2
