#!/bin/bash

DRIVER=ks.hadooplearning.task3.mapreduce.DriverHdfs2HBase
LIBJARS=$(ls -d -1 $PWD/lib/*.jar | tr "\\n" "," | sed 's/,$//')

# hbase-site.xml must be in HADOOP_CLASSPATH to working in hortonworks,
# so add 2 lines below and tail of 3rd line
HBASE_LIBS=/usr/lib/hbase/lib/
HBASE_CONF=/etc/hbase/conf/

export HADOOP_CLASSPATH=`echo ${LIBJARS} | sed s/,/:/g`:${HBASE_LIBS}:${HBASE_CONF}

yarn jar $PWD/driver/*.jar ${DRIVER} -libjars ${LIBJARS} "$@"
