drop table if exists mfc;

create table mfc(
    num int,
    location string,
    location2 string,
    address string,
    fullname string,
    wincount int,
    director string,
    website string,
    phone string,
    email string,
    longitude double,
    latitude double
)
row format serde 'org.apache.hadoop.hive.serde2.RegexSerDe'
with serdeproperties (
    "input.regex" = "^\\s*(\\d+),\"(.*)\",\"(.*)\",\"(.*)\",\"(.*)\",(\\d+),\"(.*)\",\"(.*)\",\"(.*)\",\"(.*)\",.*,(\\d*\\.?\\d*),(\\d*\\.?\\d*)\\s*$"
    )
stored as textfile;

load data local inpath '${hiveconf:INPUT}' into table mfc;

select location, max(wincount), avg(wincount), sum(wincount)
from mfc
where website is not null and length(trim(website))>0
group by location;