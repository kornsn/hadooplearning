/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ks.hadooplearing.reverseindex;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * @author ks
 */
public class Map extends Mapper<Text, Text, Text, Text> {

    private final Text word = new Text();

    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
        System.out.println("map " + key.toString() + " " + value.toString());
        StringTokenizer tokenizer = new StringTokenizer(value.toString(), (" \t\n\r\f,.:;?![](){}<>/\\'\""));
        while (tokenizer.hasMoreTokens()) {
            word.set(tokenizer.nextToken().toLowerCase());
            context.write(word, key);
        }
    }
}
