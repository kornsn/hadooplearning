/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ks.hadooplearing.reverseindex;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;

import java.io.IOException;

/**
 * Этот класс полностью аналогичен InputFormat'у, используемому по умолчанию,
 * только в качестве ключа возвращает имя файла.
 *
 * @author ks
 */
class CustomInputFormat extends FileInputFormat<Text, Text> {

    @Override
    public RecordReader<Text, Text> createRecordReader(InputSplit is, TaskAttemptContext tac) throws IOException, InterruptedException {
        RecordReader<Text, Text> reader = new CustomRecordReader();
        reader.initialize(is, tac);
        return reader;
    }

}

class CustomRecordReader extends RecordReader<Text, Text> {

    private final LineRecordReader recordReader;

    private Text key;

    public CustomRecordReader() {
        recordReader = new LineRecordReader();
    }

    @Override
    public void initialize(InputSplit is, TaskAttemptContext tac) throws IOException, InterruptedException {
        recordReader.initialize(is, tac);
        key = new Text(((FileSplit) is).getPath().getName());
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        return recordReader.nextKeyValue();
    }

    @Override
    public Text getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return recordReader.getCurrentValue();
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        return recordReader.getProgress();
    }

    @Override
    public void close() throws IOException {
        recordReader.close();
    }
}
